export type Reading = {
    katakana: string,
    hiragana: string,
    romaji: string,
    word:string
}

export type ReadingBreakdownToken = {
    katakana: string,
    hiragana: string,
    romaji: string,
}

export type ReadingBreakdown = Reading & {
    tokens: ReadingBreakdownToken[],
}

export type PracticeReadingToken = ReadingBreakdownToken & {
    input: string;
    correct: boolean | undefined;
};

export type PracticeReadingBreakdown = Reading & {
    tokens: PracticeReadingToken[],
};

export type JishoWord = {
    slug: string,
    japanese: {
        reading: string,
        word: string,
    }[],
    senses: {
        english_definitions: string[],
    }[]
}

export type KanaCharacter = {
    vowel: 'あ' | 'い' | 'う' | 'え' | 'お',
    hiragana: string,
    katakana: string,
    romaji: string,
    row: string,
}

export type ChartConfig = {
    showHiragana: boolean,
    showKatakana: boolean,
    showRomaji: boolean,
}