import type {PracticeReadingBreakdown, PracticeReadingToken, ReadingBreakdown} from "$lib/common-types";
import {getDb} from "$lib/db";

export function evaluateInput(readingBreakdown: ReadingBreakdown, input: string): PracticeReadingBreakdown {
    let cursor = 0;
    let previousCorrect = true;
    const practiceReadingBreakdown: PracticeReadingBreakdown = {...readingBreakdown, tokens: []};
    for (const token of readingBreakdown.tokens) {
        const practiceToken: PracticeReadingToken = {...token, input: '', correct: undefined};
        if (!previousCorrect) {
            const nextStart = input.indexOf(token.romaji, cursor);
            if (nextStart !== -1) {
                cursor = nextStart;
            }
        }
        if (input[cursor] === ' ' && (token.romaji === '-' || token.hiragana === 'ー')) {
            practiceToken.input = token.romaji;
            practiceToken.correct = true;
            cursor++;
            previousCorrect = true;
        } else if (input.substring(cursor, cursor + token.hiragana.length) === token.hiragana || input.substring(cursor, cursor + token.katakana.length) === token.katakana) {
            practiceToken.input = input.slice(cursor, cursor + token.hiragana.length);
            practiceToken.correct = true;
            cursor += token.hiragana.length;
            previousCorrect = true;
        } else if (checkRomaji(input, token.romaji, cursor)) {
            practiceToken.input = input.slice(cursor, cursor + token.romaji.length);
            practiceToken.correct = true;
            cursor += token.romaji.length;
            previousCorrect = true;
        } else if (cursor + token.romaji.length > input.length) {
            practiceToken.input = input.slice(cursor);
            practiceToken.correct = undefined;
            cursor += token.romaji.length;
        } else if (cursor < input.length - 1 || token.romaji.length == 1) {
            practiceToken.input = input.slice(cursor, cursor + token.romaji.length);
            practiceToken.correct = false;
            cursor += token.romaji.length;
            previousCorrect = false;
        } else {
            practiceToken.input = '';
            practiceToken.correct = undefined;
            cursor += token.romaji.length;
        }
        practiceReadingBreakdown.tokens.push(practiceToken);
    }
    return practiceReadingBreakdown;
}

function checkRomaji(input: string, expectedRomaji: string, cursor: number) {
    if ("n'" === expectedRomaji) {
        return input.startsWith("nn", cursor) || input.startsWith("n'", cursor);
    }
    if ("tsu" === expectedRomaji) {
        return input.startsWith("tsu", cursor) || input.startsWith("tu", cursor);
    }
    if ("-" === expectedRomaji || "ー" === expectedRomaji) {
        return input.startsWith("-", cursor) || input.startsWith("ー", cursor) || input.startsWith(" ", cursor);
    }
    return input.startsWith(expectedRomaji, cursor);
}

export function isKana(char: string) {
    return char.match(/[\u3040-\u309F\u30A0-\u30FF]/);
}

export function toPracticeReading(readingBreakdown: ReadingBreakdown): PracticeReadingBreakdown {
    const tokens = readingBreakdown.tokens.map(token => {
        return {
            ...token,
            input: '',
            correct: undefined
        }
    });
    return {
        ...readingBreakdown,
        tokens
    };
}

export async function getReading(word: string): Promise<ReadingBreakdown> {
    const cachedReading = await getDb().readingCache.get(word);
    if(cachedReading) {
        return cachedReading;
    }
    const response = await fetch(`/api/reading/${word}`)
    const reading = await response.json();
    reading.word = word;
    await getDb().readingCache.put(reading);
    return reading;
}