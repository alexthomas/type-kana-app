import Dexie, {type Table} from "dexie";
import type {PracticeReadingBreakdown, ReadingBreakdown} from "$lib/common-types";

export type PracticeResult = PracticeReadingBreakdown & {
    id?: number,
    startTime: number,
    endTime: number,
    correct: boolean,
    input: string,
    practiceType: 'hiragana' | 'katakana',
    backspaceCount: number,
}

export class TypeKanaDatabase extends Dexie {
    practiceResult!: Table<PracticeResult>;
    readingCache!: Table<ReadingBreakdown>;

    constructor() {
        super("typekana");
        this.version(2).stores({
            practiceResult: "++id,practiceTime"
        });
        this.version(3).stores({
            practiceResult: "++id,startTime,endTime"
        }).upgrade(tx => {
            return tx.table("practiceResult").toCollection().modify(pr => {
                pr.startTime = pr.practiceTime;
                pr.endTime = pr.practiceTime;
                delete pr.practiceTime;
            });
        });
        this.version(4).stores({
            practiceResult: "++id,startTime,endTime"
        }).upgrade(tx => {
            return tx.table("practiceResult").toCollection().modify(pr => {
               pr.word = pr.hiragana;
            });
        })
        this.version(5).stores({
            practiceResult: "++id,startTime,endTime"
        }).upgrade(tx => {
            return tx.table("practiceResult").toCollection().modify( {practiceType: 'hiragana'});
        });
        this.version(6).stores({
            practiceResult: "++id,startTime,endTime"
        }).upgrade(tx => {
            return tx.table("practiceResult").toCollection().modify( {backspaceCount: 0});
        });
        this.version(7).stores({
            practiceResult: "++id,startTime,endTime",
            readingCache: "word"
        })
    }
}

let db: TypeKanaDatabase;

export function getDb() {
    if (!db) {
        db = new TypeKanaDatabase();
    }
    return db;
}