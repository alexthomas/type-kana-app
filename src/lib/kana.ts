import type {KanaCharacter} from "$lib/common-types";
import {type Writable, writable} from "svelte/store";

export const KANA: KanaCharacter[] = [
    {vowel: 'あ', hiragana: 'あ', katakana: 'ア', romaji: 'a', row: 'あ'},
    {vowel: 'い', hiragana: 'い', katakana: 'イ', romaji: 'i', row: 'あ'},
    {vowel: 'う', hiragana: 'う', katakana: 'ウ', romaji: 'u', row: 'あ'},
    {vowel: 'え', hiragana: 'え', katakana: 'エ', romaji: 'e', row: 'あ'},
    {vowel: 'お', hiragana: 'お', katakana: 'オ', romaji: 'o', row: 'あ'},

    {vowel: 'あ', hiragana: 'か', katakana: 'カ', romaji: 'ka', row: 'か'},
    {vowel: 'い', hiragana: 'き', katakana: 'キ', romaji: 'ki', row: 'か'},
    {vowel: 'う', hiragana: 'く', katakana: 'ク', romaji: 'ku', row: 'か'},
    {vowel: 'え', hiragana: 'け', katakana: 'ケ', romaji: 'ke', row: 'か'},
    {vowel: 'お', hiragana: 'こ', katakana: 'コ', romaji: 'ko', row: 'か'},

    {vowel: 'あ', hiragana: 'さ', katakana: 'サ', romaji: 'sa', row: 'さ'},
    {vowel: 'い', hiragana: 'し', katakana: 'シ', romaji: 'shi', row: 'さ'},
    {vowel: 'う', hiragana: 'す', katakana: 'ス', romaji: 'su', row: 'さ'},
    {vowel: 'え', hiragana: 'せ', katakana: 'セ', romaji: 'se', row: 'さ'},
    {vowel: 'お', hiragana: 'そ', katakana: 'ソ', romaji: 'so', row: 'さ'},

    {vowel: 'あ', hiragana: 'た', katakana: 'タ', romaji: 'ta', row: 'た'},
    {vowel: 'い', hiragana: 'ち', katakana: 'チ', romaji: 'chi', row: 'た'},
    {vowel: 'う', hiragana: 'つ', katakana: 'ツ', romaji: 'tsu', row: 'た'},
    {vowel: 'え', hiragana: 'て', katakana: 'テ', romaji: 'te', row: 'た'},
    {vowel: 'お', hiragana: 'と', katakana: 'ト', romaji: 'to', row: 'た'},

    {vowel: 'あ', hiragana: 'な', katakana: 'ナ', romaji: 'na', row: 'な'},
    {vowel: 'い', hiragana: 'に', katakana: 'ニ', romaji: 'ni', row: 'な'},
    {vowel: 'う', hiragana: 'ぬ', katakana: 'ヌ', romaji: 'nu', row: 'な'},
    {vowel: 'え', hiragana: 'ね', katakana: 'ネ', romaji: 'ne', row: 'な'},
    {vowel: 'お', hiragana: 'の', katakana: 'ノ', romaji: 'no', row: 'な'},

    {vowel: 'あ', hiragana: 'は', katakana: 'ハ', romaji: 'ha', row: 'は'},
    {vowel: 'い', hiragana: 'ひ', katakana: 'ヒ', romaji: 'hi', row: 'は'},
    {vowel: 'う', hiragana: 'ふ', katakana: 'フ', romaji: 'fu', row: 'は'},
    {vowel: 'え', hiragana: 'へ', katakana: 'ヘ', romaji: 'he', row: 'は'},
    {vowel: 'お', hiragana: 'ほ', katakana: 'ホ', romaji: 'ho', row: 'は'},

    {vowel: 'あ', hiragana: 'ま', katakana: 'マ', romaji: 'ma', row: 'ま'},
    {vowel: 'い', hiragana: 'み', katakana: 'ミ', romaji: 'mi', row: 'ま'},
    {vowel: 'う', hiragana: 'む', katakana: 'ム', romaji: 'mu', row: 'ま'},
    {vowel: 'え', hiragana: 'め', katakana: 'メ', romaji: 'me', row: 'ま'},
    {vowel: 'お', hiragana: 'も', katakana: 'モ', romaji: 'mo', row: 'ま'},

    {vowel: 'あ', hiragana: 'ら', katakana: 'ラ', romaji: 'ra', row: 'ら'},
    {vowel: 'い', hiragana: 'り', katakana: 'リ', romaji: 'ri', row: 'ら'},
    {vowel: 'う', hiragana: 'る', katakana: 'ル', romaji: 'ru', row: 'ら'},
    {vowel: 'え', hiragana: 'れ', katakana: 'レ', romaji: 're', row: 'ら'},
    {vowel: 'お', hiragana: 'ろ', katakana: 'ロ', romaji: 'ro', row: 'ら'},

    {vowel: 'あ', hiragana: 'が', katakana: 'ガ', romaji: 'ga', row: 'が'},
    {vowel: 'い', hiragana: 'ぎ', katakana: 'ギ', romaji: 'gi', row: 'が'},
    {vowel: 'う', hiragana: 'ぐ', katakana: 'グ', romaji: 'gu', row: 'が'},
    {vowel: 'え', hiragana: 'げ', katakana: 'ゲ', romaji: 'ge', row: 'が'},
    {vowel: 'お', hiragana: 'ご', katakana: 'ゴ', romaji: 'go', row: 'が'},

    {vowel: 'あ', hiragana: 'ざ', katakana: 'ザ', romaji: 'za', row: 'ざ'},
    {vowel: 'い', hiragana: 'じ', katakana: 'ジ', romaji: 'ji', row: 'ざ'},
    {vowel: 'う', hiragana: 'ず', katakana: 'ズ', romaji: 'zu', row: 'ざ'},
    {vowel: 'え', hiragana: 'ぜ', katakana: 'ゼ', romaji: 'ze', row: 'ざ'},
    {vowel: 'お', hiragana: 'ぞ', katakana: 'ゾ', romaji: 'zo', row: 'ざ'},

    {vowel: 'あ', hiragana: 'だ', katakana: 'ダ', romaji: 'da', row: 'だ'},
    {vowel: 'い', hiragana: 'ぢ', katakana: 'ヂ', romaji: 'ji', row: 'だ'},
    {vowel: 'う', hiragana: 'づ', katakana: 'ヅ', romaji: 'zu', row: 'だ'},
    {vowel: 'え', hiragana: 'で', katakana: 'デ', romaji: 'de', row: 'だ'},
    {vowel: 'お', hiragana: 'ど', katakana: 'ド', romaji: 'do', row: 'だ'},

    {vowel: 'あ', hiragana: 'ば', katakana: 'バ', romaji: 'ba', row: 'ば'},
    {vowel: 'い', hiragana: 'び', katakana: 'ビ', romaji: 'bi', row: 'ば'},
    {vowel: 'う', hiragana: 'ぶ', katakana: 'ブ', romaji: 'bu', row: 'ば'},
    {vowel: 'え', hiragana: 'べ', katakana: 'ベ', romaji: 'be', row: 'ば'},
    {vowel: 'お', hiragana: 'ぼ', katakana: 'ボ', romaji: 'bo', row: 'ば'},

    {vowel: 'あ', hiragana: 'ぱ', katakana: 'パ', romaji: 'pa', row: 'ぱ'},
    {vowel: 'い', hiragana: 'ぴ', katakana: 'ピ', romaji: 'pi', row: 'ぱ'},
    {vowel: 'う', hiragana: 'ぷ', katakana: 'プ', romaji: 'pu', row: 'ぱ'},
    {vowel: 'え', hiragana: 'ぺ', katakana: 'ペ', romaji: 'pe', row: 'ぱ'},
    {vowel: 'お', hiragana: 'ぽ', katakana: 'ポ', romaji: 'po', row: 'ぱ'},

    {vowel: 'あ', hiragana: 'や', katakana: 'ヤ', romaji: 'ya', row: 'や'},
    {vowel: 'い', hiragana: '', katakana: '', romaji: '', row: 'や'},
    {vowel: 'う', hiragana: 'ゆ', katakana: 'ユ', romaji: 'yu', row: 'や'},
    {vowel: 'え', hiragana: '', katakana: '', romaji: '', row: 'や'},
    {vowel: 'お', hiragana: 'よ', katakana: 'ヨ', romaji: 'yo', row: 'や'},

    {vowel: 'あ', hiragana: 'わ', katakana: 'ワ', romaji: 'wa', row: 'わ'},
    {vowel: 'い', hiragana: '', katakana: '', romaji: '', row: 'わ'},
    {vowel: 'う', hiragana: 'ん', katakana: 'ン', romaji: '', row: 'わ'},
    {vowel: 'え', hiragana: '', katakana: '', romaji: '', row: 'わ'},
    {vowel: 'お', hiragana: 'を', katakana: 'ヲ', romaji: 'wo', row: 'わ'},
]

export const VOWELS = ['あ', 'い', 'う', 'え', 'お' ];
export const ROWS = ['あ', 'か', 'さ', 'た', 'な', 'は', 'ま', 'ら', 'が', 'ざ', 'だ', 'ば', 'ぱ', 'や', 'わ'];
export const hoveredCharacter:Writable<string> = writable()