import {fail, json} from "@sveltejs/kit";


export async function GET({params, fetch}) {
    const {word} = params;
    const response = await fetch(`https://highsub.alexk8s.com/v1/japanese/${word}/reading-breakdown`, {mode: 'no-cors'});
    if (!response.ok) {
        console.error(response.status)
        return json(await response.body(), fail(response.status));
    }
    return json(await response.json());
}