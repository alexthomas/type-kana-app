import {fail, json} from "@sveltejs/kit";
import type {ReadingBreakdown} from "$lib/common-types";

const wordCache = new Map<string, ReadingBreakdown>();


export async function GET({params, fetch}) { //currently returns a 403, I'm guessing that an API key is required
    const {word} = params;
    if (wordCache.has(word)) {
        return json(wordCache.get(word));
    }
    const response = await fetch(`https://jisho.org/api/v1/search/words?keyword=${word}`,{mode:'no-cors'});
    if (!response.ok) {
        console.error(response.status)
        return json(await response.text(), fail(response.status));
    }
    const data = await response.json();
    if (data.data.length === 0) {
        return json(`Word ${word} not found`, fail(404));
    }
    const reading = data.data[0];
    wordCache.set(word, reading);
    return json(reading);
}