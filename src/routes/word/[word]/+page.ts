import type {JishoWord} from "$lib/common-types";
import type {PageLoad} from "../../../../.svelte-kit/types/src/routes/word/[word]/$types";


export const load:PageLoad =({fetch, params})=> {
    //todo: load jisho word
    // const jishoWord = getJishoWord(fetch, params.word);
    const reading = getReading(fetch, params.word);
    return {reading};

}

async function getReading(fetch, word: string) {
    const response = await fetch(`/api/reading/${word}`);
    const reading =  await response.json();
    reading.word = word;
    return reading;
}

async function getJishoWord(fetch, word: string): Promise<JishoWord> {
    const response = await fetch(`/api/jisho/${word}`);
    return await response.json();

}