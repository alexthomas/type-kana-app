// You can also use the generator at https://skeleton.dev/docs/generator to create these values for you
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';
export const typeKana: CustomThemeConfig = {
	name: 'typeKana',
	properties: {
		// =~= Theme Properties =~=
		"--theme-font-family-base": `system-ui`,
		"--theme-font-family-heading": `system-ui`,
		"--theme-font-color-base": "0 0 0",
		"--theme-font-color-dark": "255 255 255",
		"--theme-rounded-base": "8px",
		"--theme-rounded-container": "8px",
		"--theme-border-base": "1px",
		// =~= Theme On-X Colors =~=
		"--on-primary": "255 255 255",
		"--on-secondary": "255 255 255",
		"--on-tertiary": "255 255 255",
		"--on-success": "0 0 0",
		"--on-warning": "0 0 0",
		"--on-error": "255 255 255",
		"--on-surface": "255 255 255",
		// =~= Theme Colors  =~=
		// primary | #353535
		"--color-primary-50": "225 225 225", // #e1e1e1
		"--color-primary-100": "215 215 215", // #d7d7d7
		"--color-primary-200": "205 205 205", // #cdcdcd
		"--color-primary-300": "174 174 174", // #aeaeae
		"--color-primary-400": "114 114 114", // #727272
		"--color-primary-500": "53 53 53", // #353535
		"--color-primary-600": "48 48 48", // #303030
		"--color-primary-700": "40 40 40", // #282828
		"--color-primary-800": "32 32 32", // #202020
		"--color-primary-900": "26 26 26", // #1a1a1a
		// secondary | #3C6E71
		"--color-secondary-50": "226 233 234", // #e2e9ea
		"--color-secondary-100": "216 226 227", // #d8e2e3
		"--color-secondary-200": "206 219 220", // #cedbdc
		"--color-secondary-300": "177 197 198", // #b1c5c6
		"--color-secondary-400": "119 154 156", // #779a9c
		"--color-secondary-500": "60 110 113", // #3C6E71
		"--color-secondary-600": "54 99 102", // #366366
		"--color-secondary-700": "45 83 85", // #2d5355
		"--color-secondary-800": "36 66 68", // #244244
		"--color-secondary-900": "29 54 55", // #1d3637
		// tertiary | #284B63
		"--color-tertiary-50": "223 228 232", // #dfe4e8
		"--color-tertiary-100": "212 219 224", // #d4dbe0
		"--color-tertiary-200": "201 210 216", // #c9d2d8
		"--color-tertiary-300": "169 183 193", // #a9b7c1
		"--color-tertiary-400": "105 129 146", // #698192
		"--color-tertiary-500": "40 75 99", // #284B63
		"--color-tertiary-600": "36 68 89", // #244459
		"--color-tertiary-700": "30 56 74", // #1e384a
		"--color-tertiary-800": "24 45 59", // #182d3b
		"--color-tertiary-900": "20 37 49", // #142531
		// success | #3ccd74
		"--color-success-50": "226 248 234", // #e2f8ea
		"--color-success-100": "216 245 227", // #d8f5e3
		"--color-success-200": "206 243 220", // #cef3dc
		"--color-success-300": "177 235 199", // #b1ebc7
		"--color-success-400": "119 220 158", // #77dc9e
		"--color-success-500": "60 205 116", // #3ccd74
		"--color-success-600": "54 185 104", // #36b968
		"--color-success-700": "45 154 87", // #2d9a57
		"--color-success-800": "36 123 70", // #247b46
		"--color-success-900": "29 100 57", // #1d6439
		// warning | #dcab18
		"--color-warning-50": "250 242 220", // #faf2dc
		"--color-warning-100": "248 238 209", // #f8eed1
		"--color-warning-200": "246 234 197", // #f6eac5
		"--color-warning-300": "241 221 163", // #f1dda3
		"--color-warning-400": "231 196 93", // #e7c45d
		"--color-warning-500": "220 171 24", // #dcab18
		"--color-warning-600": "198 154 22", // #c69a16
		"--color-warning-700": "165 128 18", // #a58012
		"--color-warning-800": "132 103 14", // #84670e
		"--color-warning-900": "108 84 12", // #6c540c
		// error | #d1311f
		"--color-error-50": "248 224 221", // #f8e0dd
		"--color-error-100": "246 214 210", // #f6d6d2
		"--color-error-200": "244 204 199", // #f4ccc7
		"--color-error-300": "237 173 165", // #edada5
		"--color-error-400": "223 111 98", // #df6f62
		"--color-error-500": "209 49 31", // #d1311f
		"--color-error-600": "188 44 28", // #bc2c1c
		"--color-error-700": "157 37 23", // #9d2517
		"--color-error-800": "125 29 19", // #7d1d13
		"--color-error-900": "102 24 15", // #66180f
		// surface | #32353e
		"--color-surface-50": "224 225 226", // #e0e1e2
		"--color-surface-100": "214 215 216", // #d6d7d8
		"--color-surface-200": "204 205 207", // #cccdcf
		"--color-surface-300": "173 174 178", // #adaeb2
		"--color-surface-400": "112 114 120", // #707278
		"--color-surface-500": "50 53 62", // #32353e
		"--color-surface-600": "45 48 56", // #2d3038
		"--color-surface-700": "38 40 47", // #26282f
		"--color-surface-800": "30 32 37", // #1e2025
		"--color-surface-900": "25 26 30", // #191a1e
	}
}